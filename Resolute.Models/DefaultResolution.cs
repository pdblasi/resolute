﻿using System;
namespace Resolute.Models
{
    public class DefaultResolution : ResolutionBase
    {
        public DefaultResolution(string name, decimal initialGoal, decimal initialProgress = 0.0M) 
            : base(ResolutionType.Default, name, initialGoal, initialProgress)
        {
        }

        protected override bool IsComplete()
        {
            return Progress >= Goal;
        }
    }
}
