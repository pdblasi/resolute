﻿using System;
namespace Resolute.Models.ScheduledResolution
{
    public class WeeklyResolution : ScheduledResolutionBase
    {
        private static decimal WeeksInYear(int year)
        {
            return new DateTime(year, 12, 31).DayOfYear / 7;
        }

        public WeeklyResolution(int year, string name, decimal initialGoal)
            : base(ScheduleInterval.Week, (int)Math.Ceiling(WeeksInYear(year)), name, Math.Ceiling(initialGoal * WeeksInYear(year)))
        {
        }

        public override DefaultResolution CurrentResolution => Resolutions[DateTime.Now.DayOfYear / 7];
    }
}
