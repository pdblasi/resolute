﻿using System;
namespace Resolute.Models.ScheduledResolution
{
    public class MonthlyResolution : ScheduledResolutionBase
    {
        public MonthlyResolution(string name, decimal initialGoal)
            : base(ScheduleInterval.Month, 12, name, initialGoal * 12)
        {
        }

        public override DefaultResolution CurrentResolution => Resolutions[DateTime.Now.Month - 1];
    }
}
