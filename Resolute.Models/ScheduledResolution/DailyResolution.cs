﻿using System;
namespace Resolute.Models.ScheduledResolution
{
    public class DailyResolution : ScheduledResolutionBase
    {
        private static int DaysInYear(int year)
        {
            return new DateTime(year, 12, 31).DayOfYear;
        }

        protected DailyResolution(int year, string name, decimal initialGoal)
            : base(ScheduleInterval.Day, DaysInYear(year), name, initialGoal * DaysInYear(year))
        {
        }

        public override DefaultResolution CurrentResolution => Resolutions[DateTime.Now.DayOfYear - 1];
    }
}
