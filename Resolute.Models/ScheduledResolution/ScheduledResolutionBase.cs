﻿using System.Linq;

namespace Resolute.Models.ScheduledResolution
{
    public abstract class ScheduledResolutionBase : ResolutionBase
    {
        public DefaultResolution[] Resolutions { get; private set; }

        public ScheduleInterval Interval { get; private set; }

        public int TotalResolutions { get; private set; }

        public int CompletedResolutions
        {
            get { return Resolutions.Count(resolution => resolution.Complete); }
        }

        public abstract DefaultResolution CurrentResolution { get; }

        protected ScheduledResolutionBase(ScheduleInterval interval, int totalResolutions, string name, decimal initialGoal)
            : base(ResolutionType.Schedule, name, initialGoal * totalResolutions)
        {
            Interval = interval;
            TotalResolutions = totalResolutions;

            Resolutions = new DefaultResolution[TotalResolutions];
            for (int i = 0; i < TotalResolutions; i++)
                Resolutions[i] = new DefaultResolution(string.Empty, initialGoal);
        }

        protected override bool IsComplete()
        {
            return Progress >= Goal;
        }

        public override void ChangeProgress(decimal amount)
        {
            base.ChangeProgress(amount);
            CurrentResolution.ChangeProgress(amount);
        }
    }

    public enum ScheduleInterval
    {
        Day,
        Week,
        Month
    }
}
