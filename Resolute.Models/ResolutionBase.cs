﻿using System;

namespace Resolute.Models
{
    public abstract class ResolutionBase
    {
        public Guid Id { get; private set; }

        public ResolutionType Type { get; private set; }

        public string Name { get; private set; }

        public decimal Progress { get; private set; }

        public decimal Goal { get; private set; }

        public bool Complete { get; protected set; }

        private ResolutionBase() {}

        protected ResolutionBase(ResolutionType type, string name, decimal initialGoal, decimal initialProgress = 0.0m)
        {
            Id = new Guid();
            Type = type;
            Name = name;
            Goal = initialGoal;
            Progress = initialProgress;
        }

        public virtual void ChangeProgress(decimal amount)
        {
            Progress += amount;
            Complete = IsComplete();
        }

        protected abstract bool IsComplete();
    }

    public enum ResolutionType
    {
        Default,
        Countdown,
        Schedule
    }
}
