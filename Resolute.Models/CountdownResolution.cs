﻿using System;
namespace Resolute.Models
{
    public class CountdownResolution : ResolutionBase
    {
        public CountdownResolution(string name, decimal initialGoal, decimal initialProgress = 0.0M)
            : base(ResolutionType.Countdown, name, initialGoal, initialProgress)
        {
        }

        protected override bool IsComplete()
        {
            return Progress <= Goal;
        }
    }
}
