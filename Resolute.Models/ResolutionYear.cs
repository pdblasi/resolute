﻿using System;
namespace Resolute.Models
{
    public class ResolutionYear
    {
        public int Year { get; private set; }

        public ResolutionBase[] Resolutions { get; private set; }

        private ResolutionYear()
        {

        }

        public ResolutionYear(int year, ResolutionBase[] resolutions)
        {
            Year = year;
            Resolutions = resolutions;
        }
    }
}
