﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PCLStorage;
using Resolute.Models;

namespace Resolute.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private const string PREVIOUS_YEARS_FILENAME = "PreviousResolutions.json";
        private const string CURRENT_YEAR_FILENAME = "CurrentResolutions.json";
        private const string NEXT_YEAR_FILENAME = "NextYearsResolutions.json";

        private CancellationTokenSource _initTokenSource = new CancellationTokenSource();

        public ViewModelBase[] ViewModels
        {
            get { return GetProperty<ViewModelBase[]>(); }
            private set { SetProperty(value); }
        }

        public int SelectedIndex
        {
            get{ return Array.IndexOf(ViewModels, Selected); }
            set { Selected = ViewModels[value]; }
        }

        public ViewModelBase Selected
        {
            get { return GetProperty<ViewModelBase>(); }
            set
            {
                if (SetProperty(value))
                {
                    RaisePropertyChanged(nameof(SelectedIndex));
                    Title = Selected.Title;
                }
            }
        }

        protected override async Task OnViewModelCreating()
        {
            var viewModels = new List<ViewModelBase>();
            var fileSystem = FileSystem.Current;
            var folder = await fileSystem.GetFolderFromPathAsync(string.Empty, _initTokenSource.Token);
            
            var previousYearTask = PopulatePreviousResolutions(folder);
            var currentYearTask = PopulateResolutions(folder, CURRENT_YEAR_FILENAME);
            var nextYearTask = PopulateResolutions(folder, NEXT_YEAR_FILENAME);

            await Task.WhenAll(previousYearTask, currentYearTask, nextYearTask);

            if (currentYearTask.Result.Year < DateTime.Now.Year)
                InitForNewYear(viewModels, previousYearTask.Result, currentYearTask.Result, nextYearTask.Result);
            else
                Init(viewModels, previousYearTask.Result, currentYearTask.Result, nextYearTask.Result);

            await Task.WhenAll(viewModels.Select(vm => vm.OnViewModelCreated()));

            ViewModels = viewModels.ToArray();
            SelectedIndex = viewModels.Count - 2;
        }

        protected override Task OnViewModelDestroying()
        {
            return Task.WhenAll(ViewModels.Select(vm => vm.OnViewModelDestroyed()));
        }

        protected override Task OnViewModelFocusing()
        {
            return Selected?.OnViewModelFocused() ?? Task.FromResult(true);
        }

        protected override Task OnViewModelUnfocusing()
        {
            return Selected?.OnViewModelUnfocused() ?? Task.FromResult(true);
        }

        private async Task<ResolutionYear> PopulateResolutions(IFolder folder, string filename)
        {
            ResolutionYear year = null;
            if ((await folder.CheckExistsAsync(filename)) == ExistenceCheckResult.FileExists)
            {
                var file = await folder.GetFileAsync(filename, _initTokenSource.Token);
                year = JsonConvert.DeserializeObject<ResolutionYear>(await file.ReadAllTextAsync());
            }

            return year;
        }

        private async Task<ResolutionYear[]> PopulatePreviousResolutions(IFolder folder)
        {
            ResolutionYear[] years = null;
            if ((await folder.CheckExistsAsync(PREVIOUS_YEARS_FILENAME)) == ExistenceCheckResult.FileExists)
            {
                var file = await folder.GetFileAsync(PREVIOUS_YEARS_FILENAME, _initTokenSource.Token);
                years = JsonConvert.DeserializeObject<ResolutionYear[]>(await file.ReadAllTextAsync());
            }

            return years;
        }

        private static void Init(List<ViewModelBase> viewModels, ResolutionYear[] previousYear, ResolutionYear currentYear, ResolutionYear nextYear)
        {
            if (previousYear == null)
                viewModels.Add(new PreviousResolutionsViewModel(previousYear));

            viewModels.Add(new ResolutionYearViewModel(currentYear, false));
            viewModels.Add(new ResolutionYearViewModel(nextYear, true));
        }

        private void InitForNewYear(List<ViewModelBase> viewModels, ResolutionYear[] previousYears, ResolutionYear currentYear, ResolutionYear nextYear)
        {
            var previousYearsList = (previousYears ?? new ResolutionYear[0]).ToList();
            previousYearsList.Add(currentYear);

            viewModels.Add(new PreviousResolutionsViewModel(previousYearsList.ToArray()));
            viewModels.Add(new ResolutionYearViewModel(nextYear, false));
            viewModels.Add(new ResolutionYearViewModel(null, true));
        }
    }
}
