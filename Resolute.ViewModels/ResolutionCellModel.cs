﻿using System;
using Resolute.Models;
using Resolute.Models.ScheduledResolution;

namespace Resolute.ViewModels
{
    public class ResolutionCellModel : NotifyPropertyChanged
    {
        private ResolutionBase _resolution;
        private ScheduledResolutionBase _scheduledResolution => ((ScheduledResolutionBase)_resolution);
        private ScheduleInterval _interval => ((ScheduledResolutionBase)_resolution).Interval;

        public string Title
        {
            get { return GetProperty<string>(); }
            private set { SetProperty(value); }
        }

        public bool ShowIntervalData
        {
            get { return _resolution is ScheduledResolutionBase; }
        }

        public string TotalLabel
        {
            get { return "Total:"; }
        }

        public string TotalStats
        {
            get { return $"{_resolution.Progress}/{_resolution.Goal}"; }
        }

        public string IntervalLabel
        {
            get { return $"This {_interval}"; }
        }

        public string IntervalStats
        {
            get { return $"{_scheduledResolution.CurrentResolution.Progress}/{_scheduledResolution.CurrentResolution.Goal}"; }
        }

        public string IntervalsLabel
        {
            get { return $"Successful {_interval}s"; }
        }

        public string IntervalsStats
        {
            get { return $"{_scheduledResolution.CompletedResolutions}/{_scheduledResolution.TotalResolutions}"; }
        }

        public ResolutionCellModel(ResolutionBase resolution, bool allowIncrement)
        {
            _resolution = resolution;
            Title = _resolution.Name;
        }

        public void Increment(object sender, EventArgs e)
        {
            _resolution.ChangeProgress(1);
            RaiseAllPropertiesChanged();
        }

        public void Decrement(object sender, EventArgs e)
        {
            _resolution.ChangeProgress(-1);
            RaiseAllPropertiesChanged();
        }
    }
}
