﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace Resolute.ViewModels
{
    public abstract class ViewModelBase : NotifyPropertyChanged
    {
        public ViewModelStatus Status
        {
            get { return GetProperty<ViewModelStatus>(); }
            protected set { SetProperty(value); }
        }

        public string Title
        {
            get { return GetProperty<string>(); }
            protected set { SetProperty(value); }
        }

        public async Task OnViewModelCreated()
        {
            await OnViewModelCreating();
            RaiseAllPropertiesChanged();
        }

        public Task OnViewModelFocused() => OnViewModelFocusing();

        public Task OnViewModelUnfocused() => OnViewModelUnfocusing();

        public async Task OnViewModelDestroyed()
        {
            ClearPropertyChangedEvent();
            await OnViewModelDestroying();
        }

        protected abstract Task OnViewModelCreating();
        protected abstract Task OnViewModelFocusing();
        protected abstract Task OnViewModelUnfocusing();
        protected abstract Task OnViewModelDestroying();
    }

    public enum ViewModelStatus
    {
        NotStarted,
        Loading,
        Blocking,
        Done,
        Succeeded,
        Failed
    }
}
