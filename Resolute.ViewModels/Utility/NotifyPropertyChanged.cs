﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Resolute.ViewModels
{
    public abstract class NotifyPropertyChanged : INotifyPropertyChanged
    {
        private Dictionary<string, object> _properties = new Dictionary<string, object>();

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected void RaiseAllPropertiesChanged()
        {
            foreach (var property in GetType().GetTypeInfo().DeclaredProperties.Where(prop => prop.GetMethod.IsPublic))
                RaisePropertyChanged(property.Name);
        }

        protected T GetProperty<T>([CallerMemberName]string propertyName = null)
        {
            return _properties.ContainsKey(propertyName) ? (T)_properties[propertyName] : default(T);
        }

        protected bool SetProperty<T>(T value, [CallerMemberName]string propertyName = null)
        {
            if (!Equals(value, GetProperty<T>(propertyName)))
            {
                _properties[propertyName] = value;
                RaisePropertyChanged(propertyName);
                return true;
            }

            return false;
        }

        protected bool SetProperty<T>(T value, ref T backing, [CallerMemberName]string propertyName = null)
        {
            if (!Equals(value, backing))
            {
                backing = value;
                RaisePropertyChanged(propertyName);
                return true;
            }

            return false;
        }

        protected void ClearPropertyChangedEvent()
        {
            PropertyChanged = null;
        }
    }
}
