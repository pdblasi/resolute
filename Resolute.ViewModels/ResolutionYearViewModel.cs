﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Resolute.Models;

namespace Resolute.ViewModels
{
    public class ResolutionYearViewModel : ViewModelBase
    {
        private ResolutionYear _resolutionYear;
        private int _year;
        private bool _nextYear;
        private ObservableCollection<ResolutionCellModel> _resolutions;

        public ReadOnlyObservableCollection<ResolutionCellModel> Resolutions
        {
            get { return GetProperty<ReadOnlyObservableCollection<ResolutionCellModel>>(); }
            private set { SetProperty(value); }
        }

        public ResolutionYearViewModel(ResolutionYear year, bool nextYear)
        {
            _resolutionYear = year;
            _year = year?.Year ?? (nextYear ? DateTime.Now.Year : DateTime.Now.Year + 1);
            _nextYear = nextYear;
        }

        protected override Task OnViewModelCreating()
        {
            _resolutions = new ObservableCollection<ResolutionCellModel>();
            Resolutions = new ReadOnlyObservableCollection<ResolutionCellModel>(_resolutions);

            Title = $"{_year} Resolutions";

            foreach (var resolution in _resolutionYear.Resolutions)
                _resolutions.Add(new ResolutionCellModel(resolution, !_nextYear));
            
            return Task.FromResult(true);
        }

        protected override Task OnViewModelDestroying()
        {
            throw new NotImplementedException();
        }

        protected override Task OnViewModelFocusing()
        {
            throw new NotImplementedException();
        }

        protected override Task OnViewModelUnfocusing()
        {
            throw new NotImplementedException();
        }
    }
}
